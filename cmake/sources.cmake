SET(SRCS 
    ${SOURCES_DIR}/main.cpp
    ${SOURCES_DIR}/imgui_impl_glfw_gl3.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/lib/glad/src/glad.c
)

SET (HDRS
    ${INCLUDE_DIR}/imgui_impl_glfw_gl3.h
)