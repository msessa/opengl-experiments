cmake_minimum_required(VERSION 2.8.12)
project(opengl_stuff)

find_package(OpenGL REQUIRED)

set(SOURCES_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src)
set(INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include)

add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/lib/glfw)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/lib/imgui)

include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/sources.cmake)

include_directories(${INCLUDE_DIR})
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/lib/glfw/include)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/lib/glad/include)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/lib/imgui)

set(sources ${SRCS} ${HDRS})
add_executable(opengl_stuff ${sources})

target_link_libraries(opengl_stuff ${OPENGL_LIBRARIES})
target_link_libraries(opengl_stuff glfw ${GLFW_LIBRARIES})
target_link_libraries(opengl_stuff imgui)
